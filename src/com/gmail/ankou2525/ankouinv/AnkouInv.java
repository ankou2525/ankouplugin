package com.gmail.ankou2525.ankouinv;

import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockFromToEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.util.Vector;

public class AnkouInv extends JavaPlugin implements Listener {
	Logger log;

	public void onEnable(){
		log = this.getLogger();
		log.info("Plugin true");
		getServer().getPluginManager().registerEvents(this, this);
	}
	public void onDisable(){
		log.info("Plugin false");
	}
	@EventHandler
	public void onBB(BlockBreakEvent e){
		if(!e.getPlayer().isOp()){
			e.setCancelled(true);
		}
	}
	@EventHandler
	public void onED(EntityDamageEvent e){
		if(e.getEntity() instanceof Player){
			if(e.getCause().equals(DamageCause.FALL)){
				e.setCancelled(true);
			}
		}
	}
	@EventHandler
	public void onPM(PlayerMoveEvent e){
		Player p = e.getPlayer();
		Location loc = p.getLocation();
		loc.setY(loc.getY()-1);
		if(loc.getBlock().getType().equals(Material.SPONGE)){
			Vector vec = p.getLocation().getDirection(); 
			p.setVelocity(vec.setY(Math.abs(vec.getY())).multiply(6).setY(6));
			p.playSound(p.getLocation(), Sound.GHAST_FIREBALL, 3, 5);
		}
		if(loc.getBlock().getType().equals(Material.LAPIS_BLOCK)){
			Vector vec = p.getLocation().getDirection(); 
			p.setVelocity(vec.setY(Math.abs(vec.getY())).multiply(6).setY(3));
			p.playSound(p.getLocation(), Sound.GHAST_FIREBALL, 3, 5);
		}
		if(loc.getBlock().getType().equals(Material.REDSTONE_BLOCK)){
			Vector vec = p.getLocation().getDirection(); 
			p.setVelocity(vec.setY(Math.abs(vec.getY())).multiply(6).setY(1));
			p.playSound(p.getLocation(), Sound.GHAST_FIREBALL, 3, 5);
		}
	}
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(cmd.getName().equalsIgnoreCase("head")){
			Player p = (Player) sender;
			if(!p.isOp()){
				p.sendMessage(ChatColor.DARK_RED+"あなたは権限がありません。");
				return false;
			}
			@SuppressWarnings("deprecation")
			ItemStack h = new ItemStack(397, 1, (short) 3);	
			SkullMeta sm = (SkullMeta) h.getItemMeta();
			sm.setOwner(args[0]);
			sm.setDisplayName(args[0]+"の頭");
			h.setItemMeta(sm);
			p.getInventory().addItem(h);
			p.sendMessage(ChatColor.AQUA+args[0]+"の頭を取得しました。");
		}
		if(cmd.getName().equalsIgnoreCase("day")){
			Player p = (Player) sender;
			if(!p.isOp()){
				p.sendMessage(ChatColor.DARK_RED+"あなたは権限がありません。");
				return false;
			}
			p.getWorld().setTime(0);
			p.sendMessage(ChatColor.AQUA+"天候を変更しました。");
		}
		if(cmd.getName().equalsIgnoreCase("cmb")){
			Player p = (Player) sender;
			if(!p.isOp()){
				p.sendMessage(ChatColor.DARK_RED+"あなたは権限がありません。");
				return false;
			}
			ItemStack i = new ItemStack(Material.COMMAND);
			p.getInventory().addItem(i);
		}
		if(cmd.getName().equalsIgnoreCase("bm")){
			Player p = (Player) sender;
			if(!p.isOp()){
				p.sendMessage(ChatColor.DARK_RED+"あなたは権限がありません。");
				return false;
			}
			Bukkit.broadcastMessage(ChatColor.AQUA+""+ChatColor.BOLD+"[Server] "+ ChatColor.LIGHT_PURPLE + ChatColor.BOLD +args[0]);
		}
		if(cmd.getName().equalsIgnoreCase("/inv")){
			Player p = (Player) sender;
			for(Player tg: Bukkit.getOnlinePlayers()){
				if(!tg.isOp()){
					p.hidePlayer(tg);
				}
			}
			p.sendMessage(ChatColor.AQUA+"プレイヤーを透明にしました。");
		}
		if(cmd.getName().equalsIgnoreCase("/show")){
			Player p = (Player) sender;
			for(Player tg: Bukkit.getOnlinePlayers()){
				p.showPlayer(tg);
			}
			p.sendMessage(ChatColor.AQUA+"プレイヤーを可視化しました。");
		}
		return false;
	}
}